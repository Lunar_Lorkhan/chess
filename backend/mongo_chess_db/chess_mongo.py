from backend.tools.mongo.db_utils import MongoDBPerformer
from hashlib import sha256
from datetime import datetime


class ChessMongo(MongoDBPerformer):
    def __init__(self, host, port, database, user_table_name, sessions_table_name, *args, **kwargs):
        super().__init__(host, port, database, *args, **kwargs)
        self.user_table_name = user_table_name
        self.sessions_table_name = sessions_table_name

    def _enctypt_password(self, password: str) -> str:
        """Use sha256 for encrypt passwords"""
        alg = sha256()
        alg.update(password.encode('utf-8'))
        return alg.hexdigest()

    def insert_user(self, login: str, password: str) -> None:
        data = {
            'login': login,
            'password': self._enctypt_password(password),
            'registration_date': datetime.now()
        }
        self._insert_values(self.user_table_name, data)

    def insert_session(self, _id, ):
        pass
