class NoDatabaseError(AttributeError):
    pass


class NoTableError(AttributeError):
    pass
