from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection
from contextlib import contextmanager
from pymongo.errors import ConnectionFailure, ConfigurationError
from backend.tools.utils import MetaSingleton


class MongoDBPerformer(metaclass=MetaSingleton):
    def __init__(self, host, port, database, *args, **kwargs):
        self.host = host
        self.port = port
        self.database = database
        self.kwargs = kwargs

    @contextmanager
    def _connect_db(self):
        client = MongoClient(self.host, self.port, **self.kwargs)
        try:
            client.admin.command('ping')
            yield client
        except (ConnectionFailure, ConfigurationError) as e:
            print('Connection failure', e, sep=' ')
        finally:
            client.close()

    def _get_db(self, client: MongoClient) -> Database:
        return getattr(client, self.database)

    def _get_document(self, db: Database, document_name: str) -> Collection:
        return getattr(db, document_name)

    def _insert_values(self, document: str, data: dict) -> None:
        with self._connect_db() as client:
            db = self._get_db(client)
            document = self._get_document(db, document)
            document.insert_one(data)
